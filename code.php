<?php
class House
{
    public $data = ['the horse and the hound and the horn that belonged to',
                    'the farmer sowing his corn that kept',
                    'the rooster that crowed in the morn that woke',
                    'the priest all shaven and shorn that married',
                    'the man all tattered and torn that kissed',
                    'the maiden all forlorn that milked',
                    'the cow with the crumpled horn that tossed',
                    'the dog that worried',
                    'the cat that killed',
                    'the rat that ate',
                    'the malt that lay in',
                    'the house that Jack built'];

    function __construct($orderer=null, $formatter=null) {
        if (!$orderer)   $orderer   = new DefaultOrderer();
        if (!$formatter) $formatter = new DefaultFormatter();
        $this->data = $formatter->format($orderer->order($this->data));
    }

    function recite() {
        $poem = [];
        foreach (range(1, sizeof($this->data())) as $i) {
            $poem[] = $this->line($i);
        }
        return join("\n", $poem);
    }

    function line($number) {
        return "This is {$this->phrase($number)}\n";
    }

    function phrase($number) {
        return join(' ', $this->parts($number));
    }

    function parts($number) {
        $phrase = [];
        foreach (range($number,0) as $i) {
            $phrase[] = $this->data()[sizeof($this->data()) - $i];
        }
        return $phrase;
    }

    function data() {
        return $this->data;
    }
}

class DefaultOrderer {
    function order($data) {
        return $data;
    }
}

class RandomOrderer {
    function order($data) {
        shuffle($data);
        return $data;
    }
}

class EchoHouse extends House {

    function __construct($orderer) {
        $this->data = array_map(function ($i) { return "$i $i"; }, $this->data);
        parent::__construct($orderer);
    }
}

class DefaultFormatter {
    function format($data) {
        return $data;
    }
}

class EchoFormatter {
    function format($data) {
        return array_map(function ($i) { return "$i $i"; }, $data);
    }
}
